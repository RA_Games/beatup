﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioSource audioSource;
    public List<AudioClip> sounds;
    public List<Color> colors;
    // Start is called before the first frame update
    void Start()
    {
        var list = GameObject.FindGameObjectsWithTag("Figure");
        foreach(GameObject go in list)
        {
            Figure f = go.GetComponent<Figure>();
            f.audioSource = audioSource;
            f.sprite.color = colors[(int)f.soundType];
            f.sound = sounds[(int)f.soundType];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public enum Sounds
{
    SOUND0,
    SOUND1,
    SOUND2,
    SOUND3,
    SOUND4,
    SOUND5,
    SOUND6,
    SOUND7
}
