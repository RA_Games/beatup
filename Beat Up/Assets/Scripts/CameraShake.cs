using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class CameraShake : MonoBehaviour
{
    private static Animator _anim;
    public static void Beat()
    {
        if (_anim == null)
            _anim = FindObjectOfType<CameraShake>().GetComponent<Animator>();

        _anim.SetTrigger("Beat");
    }
}
