﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Beat : MonoBehaviour
{
    public float speed;
    public float threshold;

    [SerializeField] UnityEvent onBeat;

    public void BeatEvent()
    {
        CameraShake.Beat();
        onBeat.Invoke();
    }
}
