﻿using UnityEngine;

public abstract class Figure : MonoBehaviour
{
    public Beat beat;
    public Sounds soundType;
    public AudioClip sound;
    public AudioSource audioSource;
    public SpriteRenderer sprite;

    public void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    //public abstract void Move(float cycleTime);
    public abstract void Move();

    private void Update()
    {
        Move();
    }

    public void PlaySound()
    {
        audioSource.PlayOneShot(sound);
    }
}
