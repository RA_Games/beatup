﻿using UnityEngine;
using System.Collections.Generic;

public class Line : Figure
{
    public List<Transform> vertex;
    public int vertexIt;
    private float time = 0;

    public override void Move()
    {
        if (beat != null)
        {
            beat.transform.position = Vector2.MoveTowards(beat.transform.position, vertex[vertexIt].position, beat.speed * Time.deltaTime);
            time += Time.deltaTime;
            if (Vector2.Distance(beat.transform.position, vertex[vertexIt].position) <= beat.threshold)
            {
                vertexIt++;
                vertexIt %= vertex.Count;
                Debug.Log(time);
                time = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Beat aux = collision.gameObject.GetComponent<Beat>();
        if(aux != null && aux != beat)
        {
            PlaySound();
            aux.BeatEvent();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Beat aux = collision.gameObject.GetComponent<Beat>();
        if (aux != null && aux != beat)
        {
            PlaySound();
            aux.BeatEvent();
        }
    }

    //[SerializeField] float halfDistance;
    /*
    bool direction;
    public override void Move(float cycleTime)
    {
        float distance = halfDistance * 2;

        float velocity = (distance / cycleTime) * Time.fixedDeltaTime;
        print(velocity);
        Vector3 top = transform.position;
        top.y += halfDistance;

        Vector3 down = transform.position;
        down.y -= halfDistance;

        if (direction)
        {
            beat.transform.position = Vector3.MoveTowards(beat.transform.position, top, velocity);

            if (Vector3.Distance(beat.transform.position, top) <= 0)
            {
                direction = !direction;
            }
        }
        else
        {
            beat.transform.position = Vector3.MoveTowards(beat.transform.position, down, velocity);

            if (Vector3.Distance(beat.transform.position, down) <= 0)
            {
                direction = !direction;
            }
        }
    }*/


    /*
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        Vector3 top = transform.position;
        top.y += halfDistance;

        Vector3 down = transform.position;
        down.y -= halfDistance;

        Gizmos.DrawLine(top, down);
    }*/
}
